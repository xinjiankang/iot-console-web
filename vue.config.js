const port = process.env.port || process.env.npm_config_port || 9001
module.exports = {
    productionSourceMap: false,
    runtimeCompiler: true,
    lintOnSave: false,

    devServer: {
		host: '0.0.0.0',
		port: port,
		open: true,
		proxy: {
			[process.env.VUE_APP_API_BASE_PATH]: {
				target: process.env.VUE_APP_API_URL,
				changeOrigin: true,
				pathRewrite: {
					['^' + process.env.VUE_APP_API_BASE_PATH]: ''
				}
			}
		},
		disableHostCheck: true
	},

    pluginOptions: {
      'style-resources-loader': {
        preProcessor: 'less',
        patterns: []
      }
    }
}
